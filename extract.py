import os
import re
from bs4 import BeautifulSoup as bs  # bs4 version 4.6.0


def get_all_files():
    ''' Traverse all directories and display only html or htm files '''
    current_directory = os.path.dirname(os.path.realpath(__file__))

    html_files = []

    for root, dirs, files in os.walk(current_directory):
        for name in files:
            path = os.path.join(root, name)

            if name.endswith(('.html', 'html')):
                html_files.append(path)

    return html_files


def main():
    all_html_files = get_all_files()

    for html in all_html_files:
        html_file = open(html, 'r')

        for data in html_file:
            bsObj = bs(data, 'html.parser')

            for link in bsObj.find_all('iframe'):
                print(link.get('src'))


if __name__ == '__main__':
    main()
